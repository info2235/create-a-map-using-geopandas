import geopandas as gpd
import matplotlib.pyplot as plt

# Read data
districts = gpd.read_file('DistrictsBerlin.geojson') # Polygons
sights = gpd.read_file('SightsBerlin.geojson') # Points

# Create a Matplotlib figure and axis for layers
fig, ax = plt.subplots(figsize=(10, 10))

# Set background color
ax.set_facecolor('magenta')

# Plot the districts in white on the axis
districts.plot(ax=ax, color='white', edgecolor='black', linewidth=1)

# Plot the sights as points
sights.plot(ax=ax, column="name", legend=True, markersize=30, label='Sights')

# Set a title for the plot
plt.title('Berlin Districts and Sights Map')

# Display the plot
plt.show()









